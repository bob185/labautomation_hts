﻿using System;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

namespace UpsideFoods
{
    // catch-all for methods and properties used throughout
    namespace Utilities
    {
        public sealed class Worklist
        {

            public const int SOURCE_PLATE_INDEX = 0;
            public const int SOURCE_PLATE_WELL_INDEX = 1;
            public const int DESTINATION_PLATE_INDEX = 2;
            public const int DESTINATION_PLATE_WELL_INDEX = 3;
            public const int VOLUME_INDEX = 4;
            public const string WORKLIST_HEADER = "SourceRack,SourceWell,DestinationRack,DestinationWell,Volume";

            public static void Write(string fileName, string text)
            {
                try
                {
                    File.WriteAllText(fileName, text);
                }
                catch(Exception ex)
                {
                    string msg = "Error writing worklist " + fileName + Environment.NewLine + ex.Message;
                }
            }

            // TODO - Talk to Addy about error handling...
            public static List<string> Read(string myFile)
            {
                return Read(myFile, WORKLIST_HEADER);
            }
            public static List<string> Read(string myFile, string header)
            {               
                List<string> worklist = new List<string>();

                //try
                //{
                var fileStream = new FileStream(myFile, FileMode.Open, FileAccess.Read);
                using (StreamReader sr = new StreamReader(fileStream, Encoding.UTF8))
                {
                    string myHeader = sr.ReadLine();
                    if (!myHeader.Equals(header))
                    {
                        string msg = "Header mismatch in the worklist, " + Environment.NewLine;
                        msg += myFile + "." + Environment.NewLine + Environment.NewLine;
                        msg += "EXPECTING:  " + Environment.NewLine + header + Environment.NewLine;
                        msg += "READING:  " + Environment.NewLine + myHeader + Environment.NewLine;
                        throw new Exception(msg);
                    }
                    while (sr.Peek() > 0)
                    {
                        worklist.Add(sr.ReadLine());
                    }
                }
                //}
                //catch(Exception ex)
                //{
                //    worklist = new ArrayList();
                //}
                return worklist;
            }
        }

        // TODO:  add logic for row-wise well counting, like Tecan.
        public static class Wells
        {
            /// <summary>
            /// Map the incoming well to the 96-well pipette
            /// Assume col-wise numeric assignment 1...12, 13...24 ...
            ///  A1(24Well) = A1(96Well)
            /// </summary>
            /// <param name="myWell"></param>
            /// Alpha-numeric well, ex A01 or A1
            /// <param name="totalWells"></param>
            /// The number of wells in the plate of the myWell well
            /// <returns></returns>
            public static int MapTo96WellFormat(string myWell, int totalWells)
            {
                int myWellId = ConvertToNumericWell(myWell, totalWells);
                int my96WellId = 0;
                int row, col;
                switch (totalWells)
                {
                    case 96:
                        my96WellId = myWellId;
                        break;
                    case 24:
                        row = (myWellId - 1) / 6;
                        col = myWellId % 6;
                        if (col == 0) col = 6;
                        my96WellId = (row * 24) + ((col * 2) - 1);
                        break;
                    case 12:
                        my96WellId = 0;
                        break;
                    case 6:
                        my96WellId = 0;
                        break;
                    default:
                        break;
                }
                return my96WellId;
            }

            // Given the alpha-numeric well, convert to a col-wise integer
            // left to right, top to bottom
            /// <summary>
            /// Convert the alpha-numeric well to a col-wise integer
            /// left to right, top to bottom 1...12, 13...24 etc
            /// </summary>
            /// <param name="well">Alpha-numeric well</param>
            /// <param name="totalWells">Totals wells in plate (96, 24, etc)</param>
            /// <returns>well numeric ID 1...n</returns>
            public static int ConvertToNumericWell(string well, int totalWells)
            {
                int wellId = 0;
                int maxColumns = 0;
                if (int.TryParse(well, out wellId))
                {
                    if ((wellId > 0) && (wellId <= totalWells)) return wellId;
                    else return -1;
                }
                maxColumns = GetMaxColumns(totalWells);

                int row = Convert.ToChar(well.Substring(0, 1).ToUpper()) - Convert.ToChar("A");
                int column = Convert.ToInt16(well.Substring(1));
                wellId = row * maxColumns + column;
                return wellId;
            }

            // Given the numeric well, convert to a col-wise (left to right, top to bottom)
            // alpha numeric notation A01...H12
            /// <summary>
            /// Convert the integer well ID to its corresponding
            /// alpha-numeric form (A01...H12), assuming col-wise well numbering
            /// left to right, top to bottom 1...12, 13...24 etc
            /// </summary>
            /// <param name="well">Numeric well ID</param>
            /// <param name="totalWells">Totals wells in plate (96, 24, etc)</param>
            /// <returns>Alpha-numeric well</returns>
            public static string ConvertToAlphaNumericWell(int well, int totalWells)
            {
                string myWell = String.Empty;
                int row = 0;
                int col = 0;
                int maxColumns = GetMaxColumns(totalWells);

                row = (well - 1) / maxColumns + 1;  // integer division
                col = well % maxColumns;
                if (col == 0) col = well / maxColumns;

                myWell = Convert.ToString(Convert.ToChar("A") + (row - 1));
                if (col < 10) myWell += "0";
                myWell += col.ToString();

                return myWell;
            }

            /// <summary>
            /// Find the max columns of a plate
            /// Totals wells in plate (96, 24, etc)
            /// </summary>
            /// <param name="totalWells">Number of well sin plate</param>
            /// <returns>number of columns in plate</returns>
            private static int GetMaxColumns(int totalWells)
            {
                int maxColumns = 0;
                switch (totalWells)
                {
                    case 96:
                        maxColumns = 12;
                        break;
                    case 24:
                        maxColumns = 6;
                        break;
                    case 12:
                        maxColumns = 6;
                        break;
                    case 6:
                        maxColumns = 3;
                        break;
                    default:
                        break;
                }
                return maxColumns;
            }
        }
    }

    namespace Exceptions
    {
        public class DuplicateIdException : Exception
        {
            public DuplicateIdException(string message) : base(message)
            {
            }
        }
    }

    namespace AssayBuilder
    {
        public static class Common
        {
            const string networkFolder = @"\\hq.memphismeats.com\r&d\Automation\HTS\Experiments\";
            const string localFolder = @"C:\Git\labautomation_hts\TestData\";
            const string filePrefix = "EXP_";

            // TODO - Add all process types here
            public static class ProcessTypes
            {
                public const int normalization = 0; 
                public const int countOnly = 1; 
                public const int bca = 2;

                static public int Normalization { get { return 0; } }
                static public int CountOnly { get { return 1; } }
                static public int BCA { get { return 2; } }
            }
            static bool useNetwork = true;
            public static bool UseNetwork
            {
                get { return useNetwork; }
                set { useNetwork = value; }
            }
            public static string ExperimentFolder
            {
                get
                {
                    if (useNetwork)
                    { return networkFolder; }
                    else { return localFolder; }
                }
            }
            public static string FilePrefix
            {
                get { return filePrefix; }
                //set { filePrefix = value; }
            }

            /// <summary>
            /// Fetch all the experiments in the local or network folder
            /// files must start with prefix and be .json
            /// </summary>
            /// <returns></returns>
            public static List<Experiment> GetExperiments()
            {
                List<Experiment> myExperiments = new List<Experiment>();
                string[] myExperimentFiles = Directory.GetFiles(Common.ExperimentFolder);
                foreach (string file in myExperimentFiles)
                {
                    string tmp = Path.GetFileName(file);
                    if((tmp.IndexOf(Common.FilePrefix)==0) && (tmp.EndsWith(".json")))
                    {
                        myExperiments.Add(new Experiment(file));
                    }
                }
                return myExperiments;
            }
        }

        public class Experiment
        {
            string description;
            DateTime creationDate;
            string labkeyId;
            string leadScientist;
            List<string> slackUsers;
            string fileName;
            Schedule schedule;
            List<string> processes;
            bool experimentComplete;

            #region Getters & Setters
            
            public string Description
            {
                get { return description; }
                set { description = value; }
            }
            public string LabKeyID
            {
                get { return labkeyId; }
                set { labkeyId = value; }
            }
            public string LeadScientist
            {
                get { return leadScientist; }
                set { leadScientist = value; }
            }
            public string FileName
            {
                get { return fileName; }
                set { fileName = value; }
            }
            public List<string> Users
            {
                get { return slackUsers; }
                set { slackUsers = value; }
            }
            public Schedule Schedule
            {
                get { return schedule; }
                set { schedule = value; }
            }
            public List<string> Processes
            {
                get { return processes; }
                set { processes = value; }
            }
            public DateTime CreationDate
            {
                get { return creationDate; }
                set { creationDate = value; }
            }
            public bool Complete
            {
                get { return experimentComplete; }
                set { experimentComplete = value; }
            }
            #endregion

            //constructor for Experiment
            //may throw exception DuplicateIdException
            /// <summary>
            /// Create an instance of an experiment
            /// </summary>
            public Experiment() { }

            /// <summary>
            /// Create an experiment
            /// </summary>
            /// <param name="desc">Experiment description</param>
            /// <param name="id">Unique ID</param>
            /// <param name="scientist">Principal investigator</param>
            /// <param name="users">List of individuals that are running the experiments and will respond to SLACK messages.</param>
            public Experiment(string desc, string id, string scientist,
                List<string> users)
            {
                creationDate = DateTime.Now;
                processes= new List<string>();
                description = desc;
                labkeyId = id;
                leadScientist = scientist;
                slackUsers = users;
                schedule = new Schedule();
                experimentComplete = false;

                //insure unique labkeyId
                fileName = Common.ExperimentFolder + Common.FilePrefix + labkeyId + ".json";
                if (File.Exists(fileName))
                {
                    throw new Exceptions.DuplicateIdException(string.Format("Unique ELN " +
                        "required"));
                }
            }
            /// <summary>
            /// Read an experiment from a file.
            /// </summary>
            /// <param name="file">Experiment json file</param>
            public Experiment(string jsonFile)
            {
                string myExperimentText = File.ReadAllText(jsonFile);
                Experiment myExperiment = JsonConvert.DeserializeObject<Experiment>(myExperimentText);

                schedule = new Schedule();
                schedule = myExperiment.schedule;
                processes = myExperiment.processes;
                description = myExperiment.description;
                labkeyId = myExperiment.labkeyId;
                leadScientist = myExperiment.leadScientist;
                slackUsers = myExperiment.slackUsers;
            }

            public void Save()
            {
                string json = JsonConvert.SerializeObject(this, Formatting.Indented);
                try
                {
                    File.WriteAllText(this.fileName, json);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            public void AddProcess(object myProcess)
            {
                string json = JsonConvert.SerializeObject(myProcess);
                Processes.Add(json);
            }

            public string GetNextProcess()
            {
                DateTime startTime = DateTime.Now.AddYears(99);
                Process myProcess = null;
                string nextProcess = null;
                foreach (string json in Processes)
                {
                    myProcess = (Process)JsonConvert.DeserializeObject<Process>(json);
                    if (!this.Complete)
                    {
                        if (myProcess.StartTime < startTime)
                        {
                            startTime = myProcess.StartTime;
                            nextProcess = json;
                        }
                    }
                }
                return nextProcess;
            }
            /// <summary>
            /// Create simple Experiment with unique ID for testing purposes
            /// </summary>
            /// <returns></returns>
            public Experiment GetExample()
            {
                string expId = DateTime.Now.ToString("HHmmss");
                Experiment myExperiment = new Experiment("my description", expId, "Addy", new List<string> { "Addy", "Bob" });
                Schedule s = new Schedule();
                Passage p = new Passage();
                string pFile= p.GetExample();
                p = new Passage(pFile);

                myExperiment.Processes.Add( JsonConvert.SerializeObject(p) );
                myExperiment.Save();

                return myExperiment;
            }
        }

        public class Passage : Process
        {
            [JsonProperty]
            string mediaBarcode;
            [JsonProperty]
            string passageBarcode;
            [JsonProperty]
            string countBarcode;
            [JsonProperty]
            string postCountBarcode;
            [JsonProperty]
            int plateCount;
            [JsonProperty]
            string countInstrument;
            [JsonProperty]
            double passageVolume;
            [JsonProperty]
            double passageDensity;
            [JsonProperty]
            int countLeadTime;
            [JsonProperty]
            bool postCount;
            [JsonProperty]
            string mediaWorklist;
            [JsonProperty]
            string sampleWorklist;

            #region Getters/Setters
            [JsonProperty]
            public string MediaBarcode
            {
                get { return mediaBarcode; }
                set { mediaBarcode = value; }
            }
            [JsonProperty]
            public string PassageBarcode
            {
                get { return passageBarcode; }
                set { passageBarcode = value; }
            }
            [JsonProperty]
            public string CountBarcode
            {
                get { return countBarcode; }
                set { countBarcode = value; }
            }
            [JsonProperty]
            public string PostCountBarcode
            {
                get { return postCountBarcode; }
                set { postCountBarcode = value; }
            }
            [JsonProperty]
            public int PlateCount
            {
                get { return plateCount; }
                set { plateCount = value; }
            }
            [JsonProperty]
            public string CountInstrument
            {
                get { return countInstrument; }
                set { countInstrument = value; }
            }
            [JsonProperty]
            public double PassageVolume
            {
                get { return passageVolume; }
                set { passageVolume = value; }
            }
            [JsonProperty]
            public double PassageDensity
            {
                get { return passageDensity; }
                set { passageDensity = value; }
            }
            [JsonProperty]
            public int CountLeadTime
            {
                get { return countLeadTime; }
                set { countLeadTime = value; }
            }
            [JsonProperty]
            public bool PostCount
            {
                get { return postCount; }
                set { postCount = value; }
            }
            [JsonProperty]
            public string MediaWorklist
            {
                get { return mediaWorklist; }
                set { mediaWorklist = value; }
            }
            [JsonProperty]
            public string SampleWorklist
            {
                get { return sampleWorklist; }
                set { sampleWorklist = value; }
            }
            #endregion

            public Passage()
            { }

            // Get a new passage to add to the passage list
            public Passage(string _experimentId, string _liquidHandlerAssay, string _sourceBarcode)
            {
                experimentId = _experimentId;
                processType = typeof(Passage);
                liquidHandlerAssay = _liquidHandlerAssay;
                sourceBarcode = _sourceBarcode;
            }

            // TODO Ask Addy if there's a better way
            public Passage(string passageFile)
            {
                Passage p = new Passage();
                p = (Passage)Parse(passageFile);
                experimentId = p.ExperimentId;
                processType = typeof(Passage);
                finishTime = p.FinishTime;
                liquidHandlerAssay = p.Assay;
                countBarcode = p.CountBarcode;
                countInstrument = p.CountInstrument;
                countLeadTime = p.CountLeadTime;
                mediaBarcode = p.mediaBarcode;
                passageDensity = p.PassageDensity;
                passageVolume = p.PassageVolume;
                plateCount = p.PlateCount;
                postCount = p.PostCount;
                postCountBarcode = p.PostCountBarcode;
                sampleWorklist = p.SampleWorklist;
                sourceBarcode = p.SourceBarcode;
                startTime = p.StartTime;
             }

            /// <summary>
            /// Read json file object, unbox in calling routine
            /// Must return an object, since inherits from process
            /// </summary>
            /// <param name="jsonFile"></param>
            /// <returns>passage instance as an object</returns>
             public override object Parse(string jsonFile)
            {
                string myPassageText = File.ReadAllText(jsonFile);
                Passage myProcess = JsonConvert.DeserializeObject<Passage>(myPassageText);
                return myProcess;
            }

            /// <summary>
            /// Write the media and sample worklists
            /// </summary>
            /// <param name="vcdData">dictionary(well,vcd)</param>
            /// <param name="mediaFileName">worklist filename for media</param>
            /// <param name="sampleFileName">worklist filename for samples</param>
            public void WriteWorklists( Dictionary<string,string> vcdData, int countVolume)
            {
                string mediaWorklist = Utilities.Worklist.WORKLIST_HEADER;
                string sampleWorklist = Utilities.Worklist.WORKLIST_HEADER;
                string well = string.Empty;
                double vcd = 0;
                double sampleVolume = 0;
                string mediaLine = string.Empty;
                string sampleLine = string.Empty;
                
                foreach (KeyValuePair<string, string> kvp in vcdData)
                {
                    well = kvp.Key;
                    vcd = Convert.ToDouble(kvp.Value);

                    // Calculate the media and samples volumes
                    // TODO what to do if sampleVolume > passageVolume?
                    sampleVolume = this.PassageVolume - countVolume;
                    if (vcd > 0) sampleVolume = this.PassageVolume * this.PassageDensity / vcd;
                    if (sampleVolume > this.PassageVolume) sampleVolume = this.PassageVolume / 2;
                    
                    sampleLine = this.SourceBarcode + "," + well + "," +
                            this.PassageBarcode + "," + well + "," + Math.Round(sampleVolume,2).ToString("0.00");
                    sampleWorklist += Environment.NewLine + sampleLine;

                    mediaLine = this.MediaBarcode + "," + well + "," + this.PassageBarcode + "," +
                             well + "," + Math.Round( (this.PassageVolume - sampleVolume), 2).ToString("0.00");
                    mediaWorklist += Environment.NewLine + mediaLine;
                }

                // Write files, update passage instance
                this.MediaWorklist = GetFileName(Common.ExperimentFolder, "media");
                Utilities.Worklist.Write(this.MediaWorklist, mediaWorklist);
                this.SampleWorklist = GetFileName(Common.ExperimentFolder, "sample");
                Utilities.Worklist.Write(this.SampleWorklist, sampleWorklist);

                
            }

            private string GetFileName(string dir, string prefix)
            {
                return dir + prefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            }
            
            //public override void Save(string jsonFile)
            //{
            //    //JsonSerializerSettings set = new JsonSerializerSettings();
            //    //set.
            //    string json = JsonConvert.SerializeObject(this, Formatting.Indented);
            //    try
            //    {
            //        File.WriteAllText(jsonFile, json);
            //    }
            //    catch (Exception ex)
            //    {
            //        //TODO write to log file
            //        Console.WriteLine(ex.Message);
            //    }
            //}
            public override string GetExample()
            {
                string id = DateTime.Now.ToString("HHmmss");
                Passage p = new Passage();
                p.Assay = "my process assay " + id;
                p.CountBarcode = "count_" + id;
                p.CountInstrument = "Cytation";
                p.CountLeadTime = 59;
                p.PassageDensity = 0.500;
                p.ExperimentId = "experiment_" + id;
                p.FinishTime = new DateTime();
                p.MediaBarcode = "media_" + id;
                p.PassageBarcode = "passage_" + id;
                p.PassageVolume = 1300;
                p.PlateCount = 99;
                p.PostCount = true;
                p.PostCountBarcode = "complete after passage";
                p.processType = typeof(Passage);
                //p.FinishTimeString = p.
                p.SourceBarcode = "source_" + id;
                p.StartTime = DateTime.Now.AddMinutes(33);

                var myFile = @"C:\Git\labautomation_hts\TestData\passageExample.json";
                p.Save(myFile);
                return myFile;
            }

        }

        public class Count : Process
        { }

        public class BCA : Process
        { }

        public class Schedule
        {
            DateTime firstRun;   // 
            DateTime mostRecentRun;
            // End of experiment occurs with a date or a total
            DateTime lastRun;
            int totalDesiredRuns;         // total desired runs
            int totalRuns;        // number of runs processed so far
            int frequency;        // hours between runs?
            bool completed;       // all scheduled experiments are complete
                                  
            List<DayOfWeek> noRunDays;
            // not sure how to repesent this idea
            // allowedShifts
            //   shift 1 => 0900-1500
            //   shift 2 => 1500-2100
            //   shift 3 => 2100-0300
            //   shift 4 => 0300-0900
            int shiftStart = 9;
            int shiftEnd = 15;

            #region Getters/Setters
            public DateTime FirstRun
            {
                get { return firstRun; }
                set { firstRun = value; }
            }
            public DateTime MostRecentRun
            {
                get { return mostRecentRun; }
                set { mostRecentRun = value; }
            }
            public DateTime LastRun
            {
                get { return lastRun; }
                set { lastRun = value; }
            }
            public int TotalDesiredRuns
            {
                get { return totalDesiredRuns; }
                set { totalDesiredRuns = value; }
            }
            public int RunCount
            {
                get { return totalRuns; }
                set { totalRuns = value; }
            }
            public int Frequency
            {
                get { return frequency; }
                set { frequency = value; }
            }
            public bool Completed
            {
                get { return completed; }
                set { completed = value; }
            }
            public List<DayOfWeek> NoRunDays
            {
                get { return noRunDays; }
                set { noRunDays = value; }
            }
            #endregion

            public Schedule()
            { }

            public DateTime GetNextRunTime()
            {
                DateTime nextRun = mostRecentRun.AddHours(frequency);

                if (totalDesiredRuns > 0)
                {
                    if (totalRuns >= TotalDesiredRuns) completed = true;
                }
                if (completed) return new DateTime();

                foreach (DayOfWeek day in noRunDays)
                {
                    if (nextRun.DayOfWeek == day) nextRun.AddHours(24);
                }
                while ((nextRun.Hour < shiftStart) || (nextRun.Hour > shiftEnd))
                {
                    nextRun.AddHours(1);
                }
                return nextRun;
            }
        }

        [JsonObject(MemberSerialization.OptIn)]
        public class Process
        {
            public string experimentId;
            public Type processType;   // needed to deserialize json string
            public string liquidHandlerAssay;
            public string sourceBarcode;
            public DateTime startTime;
            public DateTime finishTime;

            public const string dateTimeFormat = "yyyyMMMdd-HH:mm:ss";
            public CultureInfo provider = CultureInfo.InvariantCulture;

            public Process(string _experimentId, string _liquidHandlerAssay, string _sourceBarcode, DateTime _startTime, DateTime _finishTime)
            {
                experimentId = _experimentId;
                processType = typeof(Process); 
                liquidHandlerAssay = _liquidHandlerAssay;
                sourceBarcode = _sourceBarcode;
                startTime = _startTime;
                finishTime = _finishTime;
            }
            public Process()
            {
                experimentId = string.Empty;
                processType = typeof(Process);
                liquidHandlerAssay = string.Empty;
                sourceBarcode = string.Empty;
                //startTime = DateTime.MinValue;  // throws exception
                //finishTime = DateTime.MinValue;
                startTime = new DateTime();
                finishTime = new DateTime();
            }

            //
            public Process(string processFile)
            {
                Process p = new Process();
                p = (Process)Parse(processFile);
                experimentId = p.ExperimentId;
                processType = typeof(Process);
                liquidHandlerAssay = p.liquidHandlerAssay;
                sourceBarcode = p.SourceBarcode;
                startTime = p.StartTime;
                finishTime = p.FinishTime;
            }

            // Override this method for each derived process class
            public virtual Process CreateNewProcess(string _experimentId, string _liquidHandlerAssay, string _sourceBarcode, Schedule schedule)
            {
                Process nextProcess = new Process(_experimentId, _liquidHandlerAssay, _sourceBarcode, new DateTime(), new DateTime());
                nextProcess.StartTime = schedule.GetNextRunTime();
                return nextProcess;
            }
            
            /// <summary>
            /// Override for each type of process
            /// </summary>
            /// <param name="jsonFile">name of json file containing process data</param>
            /// <returns>oject cast to desired process class</returns>
            public virtual object Parse(string jsonFile)
            {
                string myProcessText = File.ReadAllText(jsonFile);
                Process myProcess = JsonConvert.DeserializeObject<Process>(myProcessText);
                return myProcess;
            }

            /// <summary>
            /// Write the process to a file
            /// </summary>
            /// <param name="jsonFile">name of json file to where process is saved</param>
            /// <returns></returns>
            public virtual void Save(string jsonFile)
            {
                string json = JsonConvert.SerializeObject(this, Formatting.Indented);
                try
                {
                    File.WriteAllText(jsonFile, json);
                }
                catch (Exception ex)
                {
                    //TODO write to log file
                    Console.WriteLine(ex.Message);
                }
            }

            public virtual string GetExample()
            {
                string id = DateTime.Now.ToString("HHmmss");
                Process p = new Process();
                p.Assay = "my process assay " + id;
                p.ExperimentId = "experiment_" + id;
                p.ProcessType = typeof(Process);
                p.FinishTime = new DateTime();
                //p.FinishTimeString = p.
                p.SourceBarcode = "source_" + id;
                p.StartTime = DateTime.Now.AddHours(99);

                var myFile = @"C:\Git\labautomation_hts\TestData\processExample.json";
                p.Save(myFile);
                return myFile;
            }

            #region Getters/Setters
            [JsonProperty]
            public string ExperimentId
            {
                get { return experimentId; }
                set { experimentId = value; }
            }
            [JsonProperty]
            public Type ProcessType
            {
                get { return processType; }
                set { processType = value; }
            }
            [JsonProperty]
            public string Assay
            {
                get { return liquidHandlerAssay; }
                set { liquidHandlerAssay = value; }
            }
            [JsonProperty]
            public string SourceBarcode
            {
                get { return sourceBarcode; }
                set { sourceBarcode = value; }
            }
            [JsonProperty]
            public DateTime StartTime
            {
                get { return startTime; }
                set { startTime = value; }
            }
            [JsonProperty]
            public string StartTimeString
            {
                get { return startTime.ToString(dateTimeFormat); }
                set { startTime = DateTime.ParseExact(value, dateTimeFormat, provider); }
            }
            [JsonProperty]
            public DateTime FinishTime
            {
                get { return finishTime; }
                set { finishTime = value; }
            }
            [JsonProperty]
            public string FinishTimeString
            {
                get { return finishTime.ToString(dateTimeFormat); }
                set { finishTime = DateTime.ParseExact(value, dateTimeFormat, provider); }
            }
            #endregion

        }
    }

    namespace Slack
    {
        public static class Globals
        {
            const string networkFolder = @"\\hq.memphismeats.com\r&d\Automation\HTS\SLACK\";
            const string localFolder = @"\\C:\UpsideFoods\HTS\SLACK\";
            static string templateFile = "SlackMessageTemplate_.txt";
            static string membersFile = "SlackUsers.txt";
            static DateTime lastMessage = new DateTime();
            static bool useNetwork = true;
            static Dictionary<string, string> memberDictionary;
            public enum MessageType
            {
                Information = 0,
                Action = 1,
                Error = 2
            }
            static public bool UseNetwork
            {
                get { return useNetwork; }
                set { useNetwork = value; }
            }
            static public string SlackFolder
            {
                get
                {
                    if (useNetwork)
                    { return networkFolder; }
                    else { return localFolder; }
                }
            }
            static public string FileTemplate
            {
                get { return SlackFolder + templateFile; }
                //set { filePrefix = value; }
            }
            static public string MembersFile
            {
                get { return SlackFolder + membersFile; }
            }
            static public DateTime LastMessageTimeStamp
            {
                get { return lastMessage; }
                set { lastMessage = value; }
            }
            static public Dictionary<string,string> MemberDictionary
            {
                get { return memberDictionary; }
                set { memberDictionary = value; }
            }
        }
        
        public class Message
        {
            /// <summary>
            /// Create a SLACK message
            /// </summary>
            /// <param name="membersToMessage">List of people to send SLCAK message</param>
            /// <param name="action">Ugency of the message</param>
            /// <param name="messageToSend">Text to send</param>
            public void PutMessageInQueue( List<string> membersToMessage, Globals.MessageType action, string messageToSend)
            {
                string myFile = string.Empty;
                string myText = string.Empty;
                var slackFileAndText = GetMessageTemplate(action);
                myFile = slackFileAndText.Item1;
                myText = slackFileAndText.Item2;

                myText = InsertMemberIds(myText, membersToMessage);
                myText = messageToSend.Replace("<message>", messageToSend);
                File.WriteAllText(myFile, myText);
                Globals.LastMessageTimeStamp = DateTime.Now;
            }

            string InsertMemberIds(string text, List<string> membersToMessage)
            {
                string myNewText = text;
                string slackIdList = string.Empty;
                List<string> missingMembers = new List<string>();
                Dictionary<string, string> registerdMembers = GetRegisterdMembers();

                foreach (string member in membersToMessage)
                {
                    if (registerdMembers.ContainsKey(member))
                    {
                        slackIdList += "<@" + registerdMembers[member] + "> ";
                    }
                    else
                    {
                        slackIdList += "<?" + member + "?> ";
                        missingMembers.Add(member);
                    }
                }
                if (missingMembers.Count > 0)
                {
                    string myErrorMessage = "Add the following member(s) to the SLACK dictionary: " + missingMembers[0];
                    foreach(string m in missingMembers.Skip(1) ) myErrorMessage += "," + m ;
                    SlackSupport(Globals.MessageType.Action, myErrorMessage);
                }

                return myNewText.Replace("<user>", slackIdList);
            }

            /// <summary>
            /// Send a SLACK to addy and bob
            /// </summary>
            /// <param name="action">Urgency of request</param>
            /// <param name="message"></param>
            public void SlackSupport( Globals.MessageType action, string message)
            {
                string myFile = string.Empty;
                string myText = string.Empty;
                var slackInfo = GetMessageTemplate(action);
                myFile = slackInfo.Item1;
                myText = slackInfo.Item2;

                myText.Replace( "<user>", "<@" + Globals.MemberDictionary["Bob"] + "> and <@" + Globals.MemberDictionary["Addy"] + ">:" );
                myText = myText.Replace("<message>", message);
                File.WriteAllText(myFile, message);
            }

            // Read file and create dictionary of <users>,<slackIds>
            // process only if the file was not previously read
             Dictionary<string, string> GetRegisterdMembers()
            {
                Dictionary<string, string> myMembers = new Dictionary<string, string>();
                myMembers = Globals.MemberDictionary;
                string[] kvp;
                try
                {
                    if ((File.GetCreationTime(Globals.MembersFile) > Globals.LastMessageTimeStamp) || (myMembers.Count <= 0))
                    {
                        string[] text = File.ReadAllLines(Globals.MembersFile);
                        foreach (string line in text.Skip(1))
                        {
                            kvp = line.Split(new char[] { ',' });
                            myMembers.Add(kvp[0], kvp[1]);
                        }
                    }
                    Globals.MemberDictionary = myMembers;
                }
                catch
                {
                    myMembers = Globals.MemberDictionary;
                    // TODO log error
                    // SLACK Bob & Addy
                }
                return myMembers;
            }

            List<string> ParseMembers(string text)
            {
                List<string> myMembers = new List<string>();
                char startChar = '<';
                char endChar = '>'; ;
                string member = string.Empty;
                int startIdx = 0;
                int endIdx = -1;

                while (startIdx >= 0)
                {
                    startIdx = text.IndexOf(startChar, startIdx);
                    if (startIdx >= 0) endIdx = text.IndexOf(endChar, startIdx);
                    if (endIdx > 0)
                    {
                        member = text.Substring(startIdx + 1, endIdx - startIdx - 1);
                        myMembers.Add(member);
                        startIdx = endIdx;
                        endIdx = -1;
                    }
                }
                return myMembers;
            }

            /// <summary>
            /// Return both the SLACK file name and the template text
            /// </summary>
            /// <param name="action">Ugency of the message</param>
            /// <returns></returns>
            Tuple<string, string> GetMessageTemplate(Globals.MessageType action)
            {
                string myFile = Globals.FileTemplate;
                string myText = string.Empty;
                if (action == Globals.MessageType.Error) { myFile = myFile.Replace("_", "_Error"); }
                else if (action == Globals.MessageType.Action) { myFile = myFile.Replace("_", "_Alert"); }
                else { myFile = myFile.Replace("_", "_Information"); }

                myText = File.ReadAllText(myFile);
                myFile = myFile.Replace("Template_", "_" + DateTime.Now.ToString("yyyyMMddHHmmss"));

                Tuple<string, string> myReturn = new Tuple<string, string>(myFile, myText);
                
                return myReturn;
            }
        }
    }

    namespace Instruments
    {
        public static class Lynx
        {
            public const string VI_HEADER = "VI;12;8";
            public const int TIP_VOLUME = 1250;
            public const string PLATE_DELIMETER = "&";
            public const string TRANSFER_DELIMETER = "%";
            private static List<string> mySourcePlates = null;   // array list of source plate labels or barcodes
            private static List<string> myWorklist = new List<string>();
            private static string myLynxTransferData = string.Empty;

            private static string myWorklistFile = @"C:\UpsideFoods\C# Projects\Lynx\LynxClass\LynxWorklist\MediaWorklist.csv";

            public static string GetStampTransfer(Dictionary<string,string> my)
            {
                string myString = string.Empty;
                return myString;
            }

                // return the string variables created from the worklist
                // assume a one-to-one mapping of a source and destination
                // src1 to dst1, src2 to dst 2, ... src_n to dst_n
                /// <summary>
                /// Given a worklist, create the Lynx string
                /// Assumes a one-to-one mapping of a source and destination plates
                ///  src1,A1 to dst1,A1, src2 to dst2, ... src_n to dst_n
                /// </summary>
                /// <param name="worklist">CSV file formated worklist</param>
                /// <returns>A string in Lynx formated worklist</returns>
                public static string GetLynxStampTransferString(string worklist)
                {
                    myWorklistFile = worklist;
                    myLynxTransferData = string.Empty;
                    // read the worklist into an arraylist and get the source plates
                    List<string> myWorklist = Utilities.Worklist.Read(worklist);
                    mySourcePlates = GetPlateList(myWorklist, Utilities.Worklist.SOURCE_PLATE_INDEX);

                    // loop over each source plate...
                    // fetch the volumes transferred for each well
                    // Dictionary <plateID, Dictionary<wellID, volumesToTransfer>>
                    Dictionary<string, Dictionary<int, List<string>>> myPlatesVolumeDic = new Dictionary<string, Dictionary<int, List<string>>>();
                    // Dictionary <wellID, volumesToTransfer>
                    Dictionary<int, List<string>> myPlateDic = new Dictionary<int, List<string>>();
                    foreach (string plate in mySourcePlates)
                    {
                        myPlateDic = GetPlateVolumes(myWorklist, plate);
                        myPlatesVolumeDic.Add(plate, myPlateDic);
                    }
                    // convert the volumes transferred into the Lynx-specific string
                    myLynxTransferData = GetPlateTransferString(myPlatesVolumeDic);

                    return myLynxTransferData;
                }

            // well, volumeArrayList
            private static Dictionary<int, List<string>> GetPlateVolumes(List<string> worklist, string plateId)
            {
                Dictionary<int, List<string>> myPlateDic = new Dictionary<int, List<string>>();
                List<string> myVolumes = new List<string>();
                string[] myDataArray = null;
                int wellId = 0;
                foreach (string line in worklist)
                {
                    myDataArray = line.Split(new char[] { ',' });
                    if (myDataArray[Utilities.Worklist.SOURCE_PLATE_INDEX] == plateId)
                    {
                        wellId = Utilities.Wells.ConvertToNumericWell(myDataArray[Utilities.Worklist.SOURCE_PLATE_WELL_INDEX], 96);
                        myVolumes = GetDispenseVolumes(myDataArray[Utilities.Worklist.VOLUME_INDEX], TIP_VOLUME);
                        myPlateDic.Add(wellId, myVolumes);
                    }
                }
                myPlateDic = NormalizeVolumes(myPlateDic);
                return myPlateDic;
            }

            // Return array list of volumes to transfer total volume required 
            // multiple transfers may be required, each transfer is an entry into the well's arraylist
            private static List<string> GetDispenseVolumes(string volume, int tipVolume)
                {
                    List<string> myVolumeList = new List<string>();
                    double myVolume, totalVolume;
                    int dispCount = 0;
                    totalVolume = Convert.ToDouble(volume);
                    myVolume = totalVolume;
                    while (myVolume > 0)
                    {
                        myVolume = myVolume - tipVolume;
                        dispCount++;
                    }
                    myVolume = totalVolume / dispCount;
                    for (int ii = 1; ii <= dispCount; ii++)
                    {
                        myVolumeList.Add(myVolume.ToString());
                    }

                    return myVolumeList;
                }

            // pass a plate of welll transfers, normalize the transfers and wells
            private static Dictionary<int, List<string>> NormalizeVolumes(Dictionary<int, List<string>> jaggedVolumes)
            {
                Dictionary<int, List<string>> myNormalizedDic = new Dictionary<int, List<string>>();
                List<string> myNormalizedVolumes = new List<string>();

                // Find max transfers for the plate
                int maxTransfers = 0;
                foreach (var value in jaggedVolumes.Values)
                {
                    if (value.Count > maxTransfers) maxTransfers = value.Count;
                }

                foreach (KeyValuePair<int, List<string>> entry in jaggedVolumes)
                {
                    myNormalizedVolumes = entry.Value;
                    if (entry.Value.Count < maxTransfers)
                    {
                        int addZeros = maxTransfers - entry.Value.Count;
                        for (int ii = 0; ii < addZeros; ii++) myNormalizedVolumes.Add("0");
                    }
                    myNormalizedDic.Add(entry.Key, new List<string>(myNormalizedVolumes));
                    myNormalizedVolumes.Clear();
                }

                // Normalize for 96-head
                List<string> noTransfers = new List<string>();
                for (int ii = 0; ii < maxTransfers; ii++) noTransfers.Add("0");

                for (int well = 1; well <= 96; well++)
                {
                    if (!myNormalizedDic.ContainsKey(well))
                    {
                        myNormalizedDic.Add(well, noTransfers);
                    }
                }
                return myNormalizedDic;
            }

            // Return an array list of desired source or destination plates in the worklist
            private static List<string> GetPlateList(List<string> worklist, int plateHeader)
            {
                List<string> myPlates = new List<string>();
                string[] lineArray = null;
                string currentPlate = string.Empty;
                foreach (string line in worklist)
                {
                    lineArray = line.Split(new char[] { ',' });
                    currentPlate = lineArray[plateHeader];
                    if (!myPlates.Contains(currentPlate)) myPlates.Add(currentPlate);
                }
                return myPlates;
            }

            // Parse the dictionary of plates/transfers into the Lynx string for processing in the Lynx method
            private static string GetPlateTransferString(Dictionary<string, Dictionary<int, List<string>>> transfers)
                {
                    string myTransferString = string.Empty;
                    string myPlateTransfers = string.Empty;
                    string myVolume = string.Empty;
                    Dictionary<int, List<string>> plateDic = new Dictionary<int, List<string>>();
                    List<string>[] myWellList = new List<string>[97];
                    int myWell = 0;

                    foreach (string srcPlate in mySourcePlates)
                    {
                        plateDic = transfers[srcPlate];
                        // Grab volume transfers and order 1...96 - the dictionary does not guaruntee order
                        for (int well = 1; well <= 96; well++) myWellList[well] = plateDic[well];

                        myPlateTransfers = PLATE_DELIMETER + VI_HEADER;
                        for (int ii = 0; ii < myWellList[1].Count; ii++)
                        {
                            for (int row = 1; row <= 8; row++)
                            {
                                for (int col = 1; col <= 12; col++)
                                {
                                    myWell = (row - 1) * 12 + col;
                                    myVolume = Convert.ToDouble(myWellList[myWell][ii]).ToString("F2");
                                    if (Convert.ToDouble(myVolume) <= 0) myVolume = "";
                                    myPlateTransfers += "," + myVolume;
                                }
                            }
                            myPlateTransfers += TRANSFER_DELIMETER + VI_HEADER;
                        }
                        // strip the last delimeter
                        myTransferString += myPlateTransfers.Substring(0, myPlateTransfers.Length - (TRANSFER_DELIMETER.Length + VI_HEADER.Length));
                    }
                    return myTransferString;
                }

             #region  Getters/Setters

                public static string GetPlates()
                {
                    string myPlates = string.Empty;
                    foreach (string plate in mySourcePlates) myPlates += "," + plate;
                    return myPlates.Substring(1);
                }

                public static int PlateCount()
                {
                    return mySourcePlates.Count;
                }
                public static string LynxTransferData()
                {
                    return myLynxTransferData;
                }
                #endregion

        }

        public sealed class Genera
        {
            private static Genera instance = null;
            private static readonly object myLockout = new object();
            Genera() { }
            public static Genera Instance
            {
                get
                {
                    lock (myLockout)
                    {
                        if (instance == null)
                        {
                            instance = new Genera();
                        }
                        return instance;
                    }
                }
            }
      
            /// <summary>
            /// Create the process specific worklists for the liquid handler to execute
            /// </summary>
            public void WriteWorklists(AssayBuilder.Process process)
            {
                AssayBuilder.Passage myPassage = (AssayBuilder.Passage)process;
                string myFile = Instruments.Cytation.GetFile(myPassage.CountBarcode);
               // Dictionary<string,string> myData = Instruments.Cytation.Instance.GetData(myPassage.CountBarcode)
            }

            //public Dictionary<string, string> GetProcessVariables(string processFile)
            //{
            //    AssayBuilder.Process nextProcess = new AssayBuilder.Process();
            //    object myObject = nextProcess.Parse(processFile);
            //    nextProcess = (AssayBuilder.Process)myObject;
            //    if (nextProcess.processClassId == AssayBuilder.Globals.ProcessTypes.Normalization)
            //    {
            //        nextProcess = null;
            //        nextProcess = new AssayBuilder.Passage();
            //        myObject = nextProcess.Parse(processFile);
            //        nextProcess = (AssayBuilder.Passage)myObject;
            //    }
            //    Dictionary<string, string> myVariables = new Dictionary<string, string>();
            //    AssayBuilder.Process nextProcess = new AssayBuilder.Process();
            //    nextProcess = JsonConvert.DeserializeObject<AssayBuilder.Process>(nextProcess.GetNextProcessFile);
            //    return myVariables;
            //}
        }

        public static class Cytation
        {
            // Snippet of the file where count data exists 
            // :
            // :
            //  Results
            //
            //  Well, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10, B11, B12, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, E1, E2, E3, E4, E5, E6, E7, E8, E9, E10, E11, E12, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11, H12,
            //  Cell Count,29,0,0,0,0,0,1,1,0,1,0,0,24,0,0,0,0,0,1,0,1,0,0,0,33,0,0,0,0,1,1,0,0,0,1,0,27,0,0,0,0,0,0,2,0,0,0,0,27,0,0,0,0,0,0,0,0,0,0,0,42,0,0,0,0,0,0,0,0,0,0,0,43,0,0,0,0,0,0,0,0,0,0,12,53,3,0,0,0,0,0,0,0,0,0,0,
            //  Dead Cell Count,2,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,
            //  Total Cell Density,0.077,0.000,0.000,0.000,0.000,0.000,0.003,0.003,0.000,0.003,0.000,0.000,0.064,0.000,0.000,0.000,0.000,0.000,0.003,0.000,0.003,0.000,0.000,0.000,0.088,0.000,0.000,0.000,0.000,0.003,0.003,0.000,0.000,0.000,0.003,0.000,0.072,0.000,0.000,0.000,0.000,0.000,0.000,0.005,0.000,0.000,0.000,0.000,0.072,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.112,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.115,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.032,0.141,0.008,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,
            //  Viable Cell Density,0.072,0.000,0.000,0.000,0.000,0.000,0.003,0.003,0.000,0.003,0.000,0.000,0.059,0.000,0.000,0.000,0.000,0.000,0.003,0.000,0.003,0.000,0.000,0.000,0.088,0.000,0.000,0.000,0.000,0.003,0.003,0.000,0.000,0.000,0.003,0.000,0.072,0.000,0.000,0.000,0.000,0.000,0.000,0.005,0.000,0.000,0.000,0.000,0.067,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.112,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.115,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.032,0.139,0.008,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,
            //  Viability,93.103,?????,???
            
            public const string TOKEN_RESULTS = "Results";   // point in the file beyond which data exists
            // Tokens indentifying comma delimited data in the file
            public const string TOKEN_WELL = "Well";
            public const string TOKEN_CELL_COUNT = "Cell Count";
            public const string TOKEN_DEAD_CELL_COUNT = "Dead Cell Count";
            public const string TOKEN_TOTAL_CELL_DENSITY = "Total Cell Density";
            public const string TOKEN_VIABLE_CELL_DENSITY = "Viable Cell Density";
            public const string TOKEN_VIABILITY = "Viability";
            public const int ROW_IDX = 1;
            public const int COL_START_IDX = 2;  // note physical plate column is columnIdx-1     
            public const int COUNT_VOLUME = 200; // sample volume required to perform count

            #region Getters/Setters
            //public string directory = @"C:\hts-upsidefoods\UpsideFoods\TestData\";
            public static string directory = @"C:\Git\labautomation_hts\TestData\";
           // public string directory = UpsideFoods.AssayBuilder.Common.ExperimentFolder;
            public static string Directory
            {
                get { return directory; }
                set
                {
                    DirectoryInfo myDirInfo = new DirectoryInfo(value);
                    if (myDirInfo.Exists) { directory = value; }
                }
            }
            
            /// <summary>
            /// Fetch the Cytation file name based off the count plate barcode
            /// </summary>
            /// <param name="barcode">Count plate barcode</param>
            /// <returns></returns>
            public static string GetFile(string barcode)
            {
                string myFile = directory + barcode + ".txt";
                return myFile;
            }
            #endregion

            /// <summary>
            /// Read Cytation output file and create a dictionary from the output data
            /// data is any of the TOKEN_KEY_nnn variables
            /// </summary>
            /// <returns> Dictionary[ well(A01...H12), data] ></returns>
            public static Dictionary<string, string> GetData(string myFile, string dataToken)
            {
                Dictionary<string, string> myData = new Dictionary<string, string>();

                string[] wells = Array.Empty<string>();
                string[]  data = Array.Empty<string>();
                int start = 0;
                string result = string.Empty;

                result = CheckOutputFile(myFile);
                if (!result.ToLower().Contains("success")) throw new Exception(result);
                try
                {
                    string[] lines = System.IO.File.ReadAllLines(myFile);
                    foreach(string line in lines)
                    {
                        if (line.Trim().Contains(TOKEN_RESULTS)) { start = 1; }
                        else if (start >= 1)
                        {

                            if (line.Contains(TOKEN_WELL))
                            {
                                wells = line.Split(new char[] { ',' });
                                start++;
                            }
                            else if (line.Contains(dataToken))
                            {
                                data = line.Split(new char[] { ',' });
                            start++;
                            }
                        }
                        if (start >= 3) break;
                    }

                    // Create dictionary<wells><data>
                    myData = new Dictionary<string, string>();

                    for (int ii = 1; ii < wells.Length; ii++)
                    {
                        if ( (wells[ii].Length > 0) & (data[ii].Length > 0) )
                        {
                            myData.Add(wells[ii], data[ii]);
                        }
                    }
                }
                catch (Exception ex)
                {
                   string msg = "Error parsing Cytation file:" + Environment.NewLine + ex.Message;
                   throw new Exception(msg);
                }
                return myData;
            }

            /// <summary>
            ///  Simple checks to make sure the file seems to be correct
            /// </summary>
            /// <param name="myFile">Cytation results file</param>
            /// <returns>success or an error message</returns>
            private static string CheckOutputFile(string myFile)
            {
                string result = "success";
                string errorMsg = string.Empty;
                string tmp = string.Empty;
                List<string> expectedTokens = new List<string> { "Reader Type:,Cytation", "Reader Serial Number:", "Software Version",
                    TOKEN_RESULTS, TOKEN_CELL_COUNT, TOKEN_DEAD_CELL_COUNT, TOKEN_TOTAL_CELL_DENSITY,
                    TOKEN_VIABILITY, TOKEN_VIABLE_CELL_DENSITY, TOKEN_WELL};

                if (!System.IO.File.Exists(myFile)) { errorMsg = "Error: Cytation file does not exist."; return errorMsg; }
                tmp = System.IO.Path.GetExtension(myFile);
                if (!tmp.Contains(".txt") && !tmp.Contains(".csv")) { errorMsg = "Cytation file must be *.txt or *.csv"; return errorMsg; }
                tmp = System.IO.File.ReadAllText(myFile);
                foreach (string token in expectedTokens)
                {
                    if (!tmp.Contains(token))
                    {
                        if (errorMsg.Length <= 0) errorMsg = "The Cytation file doesn't seem to be in the expected format. " +
                                  "Missing the following tokens:" + Environment.NewLine;
                        errorMsg += token + Environment.NewLine;
                    }
                }
                if (errorMsg.Length>0) return errorMsg;
                return result;
            }
        }

    }

    namespace Scheduler
    {
        class ProcessEvents
        {

            /// <summary>
            /// Scan through all experiments and update the the next process file
            /// </summary>
            public void UpdateNextProcess()
            {
                // For each experimnet in the folder
                // get the next process
                // bubble sort the process with earliest deadline
                // process.save Next process

            }
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            /// 



        }
    }
}
