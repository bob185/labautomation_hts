﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Globalization;
using Newtonsoft.Json;
//using UpsideFoods;

namespace Test_UpsideFoods
{
    class Program
    {
        static void Main(string[] args)
        {
            // Created by C# GUI
            string myExp = CreateExperimentFile();   

            // server creates next process file
            // scheduler opens file and, if time, executes next process
            string json = ScanExperiments(myExp);

            UpsideFoods.AssayBuilder.Process myProcess = (UpsideFoods.AssayBuilder.Process)JsonConvert.DeserializeObject<UpsideFoods.AssayBuilder.Process>(json);
            if (myProcess.ProcessType == typeof(UpsideFoods.AssayBuilder.Process))
            { 
                DoProcessClass(json);
            }
            else if (myProcess.ProcessType == typeof(UpsideFoods.AssayBuilder.Passage))
            {

                DoPassageClass(json);
            }
            else if (myProcess.ProcessType == typeof(UpsideFoods.AssayBuilder.BCA))
            {
                throw new Exception("BCA not implemented");
            }
            else
            {
                throw new Exception("Scan Experiments, use case not implemented");
            }
        }

        static void DoProcessClass(string json)
        {
            UpsideFoods.AssayBuilder.Passage myPassage = new UpsideFoods.AssayBuilder.Passage();
            myPassage = (UpsideFoods.AssayBuilder.Passage)JsonConvert.DeserializeObject<UpsideFoods.AssayBuilder.Passage>(json);

        }

        static void DoPassageClass(string json)
        {
            UpsideFoods.AssayBuilder.Passage myPassage = (UpsideFoods.AssayBuilder.Passage)JsonConvert.DeserializeObject<UpsideFoods.AssayBuilder.Passage>(json);

            // Genera reads the Cytation file and creates media and sample worklists
            // insert barcodes scanned when Lynx deck is loaded
            myPassage.PostCountBarcode = "postCount_EFG456";
            myPassage.PassageBarcode = "passage_ABC123";
            ReadCytation(myPassage);

            // Convert generic worklist to Lynx VI string
            var myMedia = CreateLynxWorklists(myPassage.MediaWorklist);
            var mySample = CreateLynxWorklists(myPassage.SampleWorklist);

            // Pass myMedia + mySample to Lynx
        }


        // The Experiment is the end result of the C# front-end
        static string CreateExperimentFile()
        {
            string myFile = string.Empty;
            UpsideFoods.AssayBuilder.Common.UseNetwork = false;

            // create an Experiment file
            UpsideFoods.AssayBuilder.Experiment myExperiment = new UpsideFoods.AssayBuilder.Experiment();
            myExperiment = myExperiment.GetExample();

            // create a process file
            UpsideFoods.AssayBuilder.Process nextProcess = new UpsideFoods.AssayBuilder.Process();
            myFile = nextProcess.GetExample();
            nextProcess = new UpsideFoods.AssayBuilder.Process(myFile);
            nextProcess.ExperimentId = myExperiment.LabKeyID;
            myExperiment.AddProcess(nextProcess);

            // create a passage file
            UpsideFoods.AssayBuilder.Passage nextPassage = new UpsideFoods.AssayBuilder.Passage();
            myFile = nextPassage.GetExample();
            nextPassage = new UpsideFoods.AssayBuilder.Passage(myFile);
            nextPassage.ExperimentId = myExperiment.LabKeyID;
            myExperiment.AddProcess(nextPassage);

            myExperiment.Save();
            return myExperiment.FileName;
        }

        static string ScanExperiments(string expFile)
        {
            UpsideFoods.AssayBuilder.Experiment myExperiment = new UpsideFoods.AssayBuilder.Experiment(expFile);
            return myExperiment.GetNextProcess();
        }

        // Grab Cytation results and write worklists
        static void ReadCytation(UpsideFoods.AssayBuilder.Passage myPassage)
        {
            // Parse data from Cytation
            string myCytFile = UpsideFoods.Instruments.Cytation.GetFile(myPassage.CountBarcode);
            myCytFile = @"C:\Git\labautomation_hts\TestData\count_132421.txt";
            Dictionary<string, string> myData = UpsideFoods.Instruments.Cytation.GetData(myCytFile, UpsideFoods.Instruments.Cytation.TOKEN_VIABLE_CELL_DENSITY);
            // Create media & sample worklists
            myPassage.WriteWorklists(myData, UpsideFoods.Instruments.Cytation.COUNT_VOLUME);
        }

        // Lynx Read worklists and creates it's own string
        static string CreateLynxWorklists(string worklist)
        {
            return UpsideFoods.Instruments.Lynx.GetLynxStampTransferString(worklist);
        }
    }
}
